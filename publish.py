import time
import paho.mqtt.client as paho
import logging

# Logging
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s > %(name)s > %(levelname)s: %(message)s')

broker = "localhost"

client = paho.Client("client-publish", clean_session=True)
client.username_pw_set(username="mosquitto", password="mosquitto")

logging.debug(f'Connecting to broker {broker}')
client.connect(broker)

message = "My 1st message"
logging.info(f"publishing: {message}")
client.publish("kaixo/mundua/mezuak", message)

time.sleep(4)

message = "My 2nd message"
logging.info(f"publishing: {message}")
client.publish("kaixo/mundua/mezuak", message)

client.disconnect()
