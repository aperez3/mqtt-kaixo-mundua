# Mosquitto

We will run ```Node-red``` and ```Mosquitto``` with docker compose:

```console
foo@bar:~$ docker-compose up
```

```Node-red``` will be on [http://localhost:1880](http://localhost:1880) and ```Mosquitto``` will be on [http://localhost:1883](http://localhost:1883)

## Node-red

Make it work:

1. Open a browser on [http://localhost:1880](http://localhost:1880)
1. Import flow.json
1. Set **username** and **password** on **mqtt-broker node** (set **mosquitto** as username&password if you are using passwdfile.txt of this repo).
1. Set topic in mqtt out and mqtt in nodes (set **mytopic** if you want to interact with python scripts).

Node-red will have a flow publishing messages and another one reading them.

If we inject a message, debug should show ```nodered message```.

## Python scripts

We have 2 python scripts:

1. ```subscribe.py```: a MQTT client that connects to Mosquitto and listens to messages published with **mytopic** topic.
1. ```publish.py```: a MQTT client that connects to Mosquitto and publishes 2 messages with **mytopic** topic.

They use **paho-mqtt** library, we re goint to use **venv** to install it.

### Paho-mqtt library

We need [paho-mqtt](https://pypi.org/project/paho-mqtt/) library to run the python scripts.

In order to make it run we will generate a virtual environment:

```console
foo@bar:~$ python3 -m venv mqtt-env
```

Enter it:

```console
foo@bar:~$ source mqtt-env/bin/activate
(mqtt-env) foo@bar:~$
```

Install ```paho-mqtt```:

```console
(mqtt-env) foo@bar:~$ pip install paho-mqtt
```

### Making it work

If you are not in the virtual environment, enter it:

```console
foo@bar:~$ source mqtt-env/bin/activate
(mqtt-env) foo@bar:~$
```

Now we can execute the subscriber:

```console
(mqtt-env) foo@bar:~$ python subscribe.py
connecting to broker localhost
subscribing
```

Execute the publisher:

```console
(mqtt-env) foo@bar:~$ python publish.py 
connecting to broker  localhost
publishing message 1
publishing message 2
(mqtt-env) foo@bar:~$
```

Subscriber should show:

```console
connecting to broker  localhost
subscribing 
received message =  mymessage
received message =  my second message
```

## Security

This repository has an example passwdfile.txt file with an example user:

* **username**: mosquitto
* **password**: mosquitto

In order to create a new password file we have to enter to ```mosquitto``` docker, create the password file and extract it *(container id should be extracted with ```docker ps```, **b9328ab88107** in the example):*

```console
foo@bar:~$ docker-compose exec mosquitto /bin/sh
/ # mosquitto_passwd -c passwdfile.txt user
Password: 
Reenter password: 
/ # exit
foo@bar:~$ docker cp b9328ab88107:/passwdfile.txt mosquitto/passwdfile.txt
```

Then you should update the username and passwords in ```node-red``` and ```python``` apps.

More info at [http://www.steves-internet-guide.com/mqtt-username-password-example/](http://www.steves-internet-guide.com/mqtt-username-password-example/)
